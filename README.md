# insomnia tokenize plugin

Keep token alive with the variable name `tokenVarName` environment variable, if it is not setted it takes `token` by default.

Your API needs to use "tokenVarName" header in response to work with this plugin.

## environment configuration

```json
{
  "tokenVarName": "token"
}
```


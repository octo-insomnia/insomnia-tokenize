module.exports = function( context ) {
  let workspaceId = JSON.parse( localStorage.getItem( 'insomnia::meta::activeWorkspaceId' ) )
  let tokenVarName = context.request.getEnvironmentVariable( 'tokenVarName' ) || 'token'
  let storageName = workspaceId + tokenVarName
  let token = context.response.getHeader( tokenVarName )
  if ( token )
    localStorage.setItem( storageName, token )
}

